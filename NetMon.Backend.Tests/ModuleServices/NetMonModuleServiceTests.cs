﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetMon.Backend.Models;
using NetMon.Backend.ModuleServices;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace NetMon.Backend.Tests.ModuleServices
{
    /// <summary>
    /// Summary description for NetMonModulServiceTests
    /// </summary>
    [TestClass]
    public class NetMonModuleServiceTests : NetMonModuleService
    {
        public NetMonModuleServiceTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public async Task LoadConfig_PathToValidXML_ResultIsTrueAndObjectIsPool()
        {
            UniversalResult result = null;
            string path = $"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfig.xml";

            result = await LoadConfig(path);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<NetworkNodePool>());
        }

        [TestMethod]
        public async Task LoadConfig_PathToValidXML_NetworkNodePoolIsCorrect()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;
            string path = $"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfig.xml";

            result = await LoadConfig(path);
            networkNodePool = result.ObjectAs<NetworkNodePool>();

            Assert.IsTrue(networkNodePool.Pool.Count == 4);
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_PoolIsNull_ResultIsFalse()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;

            result = await CheckPoolConnectivity(networkNodePool);

            Assert.IsFalse(result.ResultStatus);
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_PoolIsNotNullButEmpty_ResultIsFalse()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = new NetworkNodePool();

            result = await CheckPoolConnectivity(networkNodePool);

            Assert.IsFalse(result.ResultStatus);
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_PoolIsNotEmpty_ResultIsTrueAndObjectIsStatus()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = new NetworkNodePool();
            NetworkNode networkNode = new NetworkNode(IPAddress.Loopback.ToString(), "Localhost");
            networkNodePool.Add(networkNode);

            result = await CheckPoolConnectivity(networkNodePool);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<NetworkStatus>());
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_OnlyNodeIsLocalhost_PoolIsConnected()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = new NetworkNodePool();
            NetworkNode networkNode = new NetworkNode(IPAddress.Loopback.ToString(), "Localhost");
            networkNodePool.Add(networkNode);

            result = await CheckPoolConnectivity(networkNodePool);
            networkNodePool.Status = result.ObjectAs<NetworkStatus>();

            Assert.IsTrue(networkNodePool.IsConnected());
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_LoadPoolFromMockData_PoolWasConnected()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;
            string path = $"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfig.xml";

            result = await LoadConfig(path);
            networkNodePool = result.ObjectAs<NetworkNodePool>();

            result = await CheckPoolConnectivity(networkNodePool);
            networkNodePool.Status = result.ObjectAs<NetworkStatus>();

            Assert.IsTrue(networkNodePool.WasConnected());
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_LoadPoolFromMockData_PoolIsNotConnectedAndErrorAtRouter()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;
            string path = $"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfigWithError.xml";

            result = await LoadConfig(path);
            networkNodePool = result.ObjectAs<NetworkNodePool>();

            result = await CheckPoolConnectivity(networkNodePool);
            networkNodePool.Status = result.ObjectAs<NetworkStatus>();

            Assert.IsFalse(networkNodePool.IsConnected());
            Assert.IsTrue(networkNodePool.Status.ErrorAt.Name == "Router");
        }

        [TestMethod]
        public async Task CheckPoolConnectivity_ChangeIpFromInvalidToValid_StatusHasChanged()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;
            string path = $"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfigWithError.xml";

            result = await LoadConfig(path);
            networkNodePool = result.ObjectAs<NetworkNodePool>();

            result = await CheckPoolConnectivity(networkNodePool);
            networkNodePool.Status = result.ObjectAs<NetworkStatus>();

            Assert.IsFalse(networkNodePool.IsConnected());

            networkNodePool.GetRootNode().Ip = "192.168.2.1";

            result = await CheckPoolConnectivity(networkNodePool);
            networkNodePool.Status = result.ObjectAs<NetworkStatus>();

            Assert.IsTrue(networkNodePool.IsConnected());
            Assert.IsTrue(networkNodePool.Status.HasChanged);
        }

        [TestMethod]
        public async Task CreateDefaultNetworkNodePool_NetworkNodePoolIsCorrect()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = null;
            string gatewayIp = string.Empty;

            result = await CreateDefaultNetworkNodePool();
            networkNodePool = result.ObjectAs<NetworkNodePool>();

            Assert.AreEqual("8.8.8.8", networkNodePool.GetRootNode().Ip);
            Assert.AreEqual("8.8.6.6", networkNodePool.GetRootNode().IpAlternative);
            Assert.AreEqual("192.168.2.1", networkNodePool.GetRootNode().FollowingNode.Ip);
        }
    }
}
