﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices;
using System.Net;
using System.Threading.Tasks;

namespace NetMon.Backend.Tests.ModelServices
{
    /// <summary>
    /// Summary description for NetworkModelServiceTests
    /// </summary>
    [TestClass]
    public class NetworkModelServiceTests : NetworkModelService
    {
        public NetworkModelServiceTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public async Task IsAccessible_IpIsNull_ResultIsFalse()
        {
            IPAddress ip = null;
            UniversalResult result = await IsAccessible(ip);
            Assert.IsFalse(result.ResultStatus);
        }

        [TestMethod]
        public async Task IsAccessible_IpIsLocalhost_ResultIsTrue()
        {
            IPAddress ip = IPAddress.Loopback;
            UniversalResult result = await IsAccessible(ip);
            Assert.IsTrue(result.ResultStatus);
        }

        [TestMethod]
        public async Task IsAccessible_IpIsGoogleDNS_ResultIsTrue()
        {
            IPAddress ip = IPAddress.Parse("8.8.8.8");
            UniversalResult result = await IsAccessible(ip);
            Assert.IsTrue(result.ResultStatus);
        }

        [TestMethod]
        public async Task GetGatewayIp_ResultObjectIsIPAddress()
        {
            UniversalResult result = await GetGatewayIp();
            Assert.IsTrue(result.ObjectAs<IPAddress>() is IPAddress);
        }

        [TestMethod]
        public async Task GetGatewayIp_IpIsCorrect()
        {
            UniversalResult result = await GetGatewayIp();
            string ip = string.Empty;

            if (result.IsSuccessAndObjectIsType<string>())
                ip = result.ObjectAs<string>();

            Assert.IsTrue(ip == "192.168.2.1");
        }
    }
}
