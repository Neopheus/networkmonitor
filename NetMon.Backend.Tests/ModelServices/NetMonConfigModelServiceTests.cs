﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices;
using System.Threading.Tasks;
using System.IO;

namespace NetMon.Backend.Tests.ModelServices
{
    /// <summary>
    /// Summary description for NetMonConfigModelServiceTests
    /// </summary>
    [TestClass]
    public class NetMonConfigModelServiceTests : NetMonConfigModelService
    {
        public NetMonConfigModelServiceTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public async Task SerializeToXML_ConfigIsNull_ResultIsFalse()
        {
            UniversalResult result = null;
            NetMonConfig config = null;

            result = await SerializeToXML(config);

            Assert.IsFalse(result.ResultStatus);
        }

        [TestMethod]
        public async Task SerializeToXML_ConfigIsNotNullNodeIsNull_ResultIsTrueAndObjectString()
        {
            UniversalResult result = null;
            NetworkNode node = null;
            NetMonConfig config = new NetMonConfig(node);

            result = await SerializeToXML(config);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<string>());
        }

        [TestMethod]
        public async Task SerializeToXML_ConfigIsNotNullNodeIsNotNull_ResultIsTrueAndObjectString()
        {
            UniversalResult result = null;
            NetworkNode node = new NetworkNode("192.169.2.5", "test", "only for testing");
            NetMonConfig config = new NetMonConfig(node);

            result = await SerializeToXML(config);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<string>());
        }

        [TestMethod]
        public async Task SerializeToXML_ConfigIsNotNullNodeIsNotNull_SameConfigAfterSerialationAndDeserialation()
        {
            UniversalResult result = null;
            NetworkNode node = new NetworkNode("192.169.2.5", "test", "only for testing");
            NetMonConfig config = new NetMonConfig(node);

            result = await SerializeToXML(config);
            string xmlAsString = result.ObjectAs<string>();

            result = await DeserializeFromXML(xmlAsString);
            NetMonConfig newConfig = result.ObjectAs<NetMonConfig>();

            Assert.IsTrue(config.RootNode.Ip == newConfig.RootNode.Ip);
        }

        [TestMethod]
        public async Task SerializeToXML_CompletePool_ResultIsTrueAndObjectString()
        {
            UniversalResult result = null;
            NetworkNode nodeA = new NetworkNode()
            {
                Name = "Internet",
                Description = "The Internet.",
                Ip = "8.8.8.8",
                IpAlternative = "8.8.6.6",
                PreviousNode = null,
                FollowingNode = null
            };
            NetworkNode nodeB = new NetworkNode()
            {
                Name = "Router",
                Description = "The Router.",
                Ip = "192.168.2.1",
                IpAlternative = null,
                PreviousNode = null,
                FollowingNode = null
            };
            NetworkNode nodeC = new NetworkNode()
            {
                Name = "Bridge",
                Description = "The Bridge.",
                Ip = "192.168.2.2",
                IpAlternative = null,
                PreviousNode = null,
                FollowingNode = null
            };
            NetworkNode nodeD = new NetworkNode()
            {
                Name = "AccessPoint",
                Description = "The AccessPoint.",
                Ip = "192.168.2.3",
                IpAlternative = null,
                PreviousNode = null,
                FollowingNode = null
            };
            nodeA.SetFollowingNode(nodeB);
            nodeB.SetFollowingNode(nodeC);
            nodeC.SetFollowingNode(nodeD);
            NetworkNodePool netNodePool = new NetworkNodePool();
            netNodePool.Pool.AddRange(new NetworkNode[] { nodeA, nodeB, nodeC, nodeD });
            NetMonConfig config = new NetMonConfig(netNodePool);

            result = await SerializeToXML(config);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<string>());
        }

        [TestMethod]
        public async Task DeserialzeFromXML_StringIsEmpty_ResultIsFalse()
        {
            UniversalResult result = null;
            string xmlAsString = string.Empty;

            result = await DeserializeFromXML(xmlAsString);

            Assert.IsFalse(result.ResultStatus);
        }

        [TestMethod]
        public async Task DeserialzeFromXML_StringIsValidXML_ResultIsTrueAndObjectNetMonConfig()
        {
            UniversalResult result = null;
            string xmlAsString = File.ReadAllText($"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfig.xml");

            result = await DeserializeFromXML(xmlAsString);

            Assert.IsTrue(result.IsSuccessAndObjectIsType<NetMonConfig>());
        }

        [TestMethod]
        public async Task DeserialzeFromXML_StringIsValidXML_NetworkNodePoolIsCorrect()
        {
            UniversalResult result = null;
            NetMonConfig config = null;
            NetworkNodePool networkNodePool = null;
            string xmlAsString = File.ReadAllText($"{Directory.GetCurrentDirectory()}\\..\\..\\MockData\\NetMonConfig.xml");

            result = await DeserializeFromXML(xmlAsString);
            if (result.IsSuccessAndObjectIsType<NetMonConfig>())
                config = result.ObjectAs<NetMonConfig>();

            networkNodePool = new NetworkNodePool(config);
            Assert.IsTrue(networkNodePool.Pool.Count == 4);
        }
    }
}
