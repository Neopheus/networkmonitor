﻿using NetMon.Backend.ModuleServices;
using NetMon.Backend.ModuleServices.Interfaces;

namespace NetMon.Frontend.TrayIcon.Bootstrap
{
    internal class InitApplicationServices
    {
        #region Modul Services
        internal static INetMonModuleService GetNetMonModuleService() => new NetMonModuleService();
        #endregion Modul Services
    }
}
