﻿using NetMon.Backend.Models;
using NetMon.Backend.ModuleServices.Interfaces;
using NetMon.Frontend.TrayIcon.Bootstrap;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetMon.Frontend.TrayIcon
{
    static class Program
    {
        #region Modules

        private static INetMonModuleService NetMonModuleService;
        private static INetMonModuleService GetCurrentNetMonModuleService()
        {
            if (NetMonModuleService == null)
                NetMonModuleService = InitApplicationServices.GetNetMonModuleService();
            return NetMonModuleService;
        }

        #endregion Modules

        private static readonly string APPNAME = Application.ProductName;
        private static readonly int UPDATEINTRVAL = 10;
        private static Timer timer = null;
        private static NotifyIcon notyIcon = null;

        private static readonly Icon iconWhite = Properties.Resources.netmon_icon_filled_white;
        private static readonly Icon iconGreen = Properties.Resources.netmon_icon_filled_green;
        private static readonly Icon iconRed = Properties.Resources.netmon_icon_filled_red;

        private static NetworkNodePool networkNodePool = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            notyIcon = CreateNotifyIcon();
            timer = CreateTimer();

            Start();
            Application.EnableVisualStyles();
            Application.Run();
        }

        #region EventHandler

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Icon_SingleClick(Object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Icon_DoubleClick(Object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Item_Open_Click(Object sender, EventArgs e)
        {
            Open();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void Item_Load_Click(Object sender, EventArgs e)
        {
            await Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Item_Exit_Click(Object sender, EventArgs e)
        {
            notyIcon.Dispose();
            Application.Exit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Item_Start_Click(Object sender, EventArgs e)
        {
            Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Item_Stop_Click(Object sender, EventArgs e)
        {
            Stop();
        }

        #endregion EventHandler

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        private static void Open()
        {
            MessageBox.Show("Not yet implemented!", "NetMon - Open");
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Start()
        {
            if (timer?.Enabled == false)
            {
                networkNodePool?.ResetStatus();
                SetTrayIcon("Starting...", IconStyle.Normal);
                timer.Start();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Stop()
        {
            if (timer?.Enabled == true)
            {
                SetTrayIcon("Stopped", IconStyle.Normal);
                timer.Stop();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static async Task Load()
        {
            UniversalResult result = null;

            var fileDialog = new OpenFileDialog()
            {
                FileName = "NetMonConfig",
                DefaultExt = ".xml",
                Filter = "XML documents (.xml)|*.xml"
            };

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                Stop();

                result = await GetCurrentNetMonModuleService().LoadConfig(fileDialog.FileName);
                if (result?.ResultStatus == true && result.ResultObject != null)
                    networkNodePool = result.ObjectAs<NetworkNodePool>();
                else
                    notyIcon.ErrorHandling(result, timer);

                result = await GetCurrentNetMonModuleService().SaveConfig(networkNodePool);
                if (result?.ResultStatus == true)
                    ShowMessage("Config file successfully loaded.", MessageStyle.Success);
                else
                    notyIcon.ErrorHandling(result, timer);

                if (networkNodePool is NetworkNodePool)
                    Start();
            }
            else
                ShowMessage("Loading config file cancled.", MessageStyle.Error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void Update(Object sender, EventArgs e)
        {
            UniversalResult result = null;

            try
            {
                result = await GetCurrentNetMonModuleService().LoadAndCheckPoolConnectivity(networkNodePool);
                if (result.IsSuccessAndObjectIsType<NetworkNodePool>())
                    networkNodePool = result.ObjectAs<NetworkNodePool>();
                else
                    notyIcon.ErrorHandling(result, timer);

                notyIcon.StatusHandling(networkNodePool);
            }
            catch (Exception ex)
            {
                notyIcon.ErrorHandling(UniversalResult.DefaultErrorResult(ex.Message), timer);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static NotifyIcon CreateNotifyIcon()
        {
            NotifyIcon notyIcon = new NotifyIcon
            {
                Icon = iconWhite,
                Text = "NetMon",
                Visible = true,
                ContextMenu = CreateContextMenu()
            };
            notyIcon.DoubleClick += new EventHandler(Icon_DoubleClick);
            return notyIcon;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static ContextMenu CreateContextMenu()
        {
            ContextMenu cm = new ContextMenu();
            MenuItem menuItem;
            int itemIndex = 0;

            menuItem = new MenuItem
            {
                Index = itemIndex++,
                Text = "&Open"
            };
            menuItem.Click += new EventHandler(Item_Open_Click);
            cm.MenuItems.Add(menuItem);

            menuItem = new MenuItem
            {
                Index = itemIndex++,
                Text = "&Load"
            };
            menuItem.Click += new EventHandler(Item_Load_Click);
            cm.MenuItems.Add(menuItem);

            menuItem = new MenuItem
            {
                Index = itemIndex++,
                Text = "&Start"
            };
            menuItem.Click += new EventHandler(Item_Start_Click);
            cm.MenuItems.Add(menuItem);

            menuItem = new MenuItem
            {
                Index = itemIndex++,
                Text = "&Stop"
            };
            menuItem.Click += new EventHandler(Item_Stop_Click);
            cm.MenuItems.Add(menuItem);

            menuItem = new MenuItem
            {
                Index = itemIndex++,
                Text = "&Quit"
            };
            menuItem.Click += new EventHandler(Item_Exit_Click);
            cm.MenuItems.Add(menuItem);

            return cm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static Timer CreateTimer()
        {
            Timer timer = new Timer
            {
                Interval = (UPDATEINTRVAL * 1000)
            };
            timer.Tick += new EventHandler(Update);
            return timer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        private static NotifyIcon StatusHandling(this NotifyIcon notyIcon, NetworkNodePool networkNodePool)
        {
            if (networkNodePool is NetworkNodePool && networkNodePool.Status.HasChanged)
            {
                string ip = string.Empty;
                string name = string.Empty;
                string time = string.Empty;

                if (networkNodePool.IsConnected())
                {
                    ip = networkNodePool.GetRootNode().Ip;
                    name = networkNodePool.GetRootNode().Name;
                    time = networkNodePool.Status.LastChange.ToString("HH:mm dd.MM.yyyy");
                    SetTrayIcon($"Connected to {name}\n{time}", IconStyle.Success);
                    ShowMessage($"Connected to {name}\n({time})", MessageStyle.Success);
                }
                else
                {
                    ip = networkNodePool.Status.ErrorAt.Ip;
                    name = networkNodePool.Status.ErrorAt.Name;
                    time = networkNodePool.Status.LastChange.ToString("HH:mm dd.MM.yyyy");
                    SetTrayIcon($"No connection to {name}\n{time}", IconStyle.Error);
                    ShowMessage($"Currently no connection to {name} ({ip})\n({time})", MessageStyle.Error);
                }
            }
            return notyIcon;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        private static NotifyIcon ErrorHandling(this NotifyIcon notyIcon, UniversalResult result, Timer timer)
        {
            if (result is UniversalResult)
            {
                SetTrayIcon($"Stopped because an error has occured.", IconStyle.Error);
                ShowMessage($"An error has occured: {result.Message} (Code {result.ErrorCode})", MessageStyle.Error);
                if (timer is Timer) timer.Stop();
            }
            return notyIcon;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageStyle"></param>
        private static void ShowMessage(string message = "", MessageStyle messageStyle = MessageStyle.Notification)
        {
            notyIcon.BalloonTipTitle = $"{APPNAME} - {messageStyle}";
            notyIcon.BalloonTipText = $"{message}";
            notyIcon.ShowBalloonTip(1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="iconStyle"></param>
        private static void SetTrayIcon(string message = "", IconStyle iconStyle = IconStyle.Normal)
        {
            Icon icon = null;
            switch (iconStyle)
            {
                case IconStyle.Normal:
                    icon = iconWhite;
                    break;
                case IconStyle.Success:
                    icon = iconGreen;
                    break;
                case IconStyle.Error:
                    icon = iconRed;
                    break;
                default:
                    icon = iconWhite;
                    break;
            }
            notyIcon.Icon = icon;
            notyIcon.Text = $"{APPNAME} - {message}";
        }

        #endregion Methods

        #region Enums

        /// <summary>
        /// 
        /// </summary>
        enum IconStyle
        {
            Normal,
            Success,
            Error
        }

        /// <summary>
        /// 
        /// </summary>
        enum MessageStyle
        {
            Notification,
            Success,
            Error
        }

        #endregion Enums
    }
}
