﻿using NetMon.Backend.ModelServices;
using NetMon.Backend.ModelServices.Interfaces;
using NetMon.Backend.ModuleServices;
using NetMon.Backend.ModuleServices.Interfaces;

namespace NetMon.Backend.Bootstrap
{
    /// <summary>
    /// 
    /// </summary>
    internal class InitApplicationServices
    {
        #region Model Services
        internal static IReadFileModelService GetReadFileModelService() => new ReadFileModelService();
        internal static IWriteFileModelService GetWriteFileModelService() => new WriteFileModelService();
        internal static IListFilesModelService GetListFilesModelService() => new ListFilesModelService();
        internal static IProcessModelService GetProcessModelService() => new ProcessModelService();
        internal static INetMonConfigModelService GetNetMonConfigModelService() => new NetMonConfigModelService();
        internal static INetworkModelService GetNetworkModelService() => new NetworkModelService();
        #endregion Model Services

        #region Modul Services
        internal static INetMonModuleService GetNetMonModuleService() => new NetMonModuleService();
        #endregion Modul Services

        #region Repository Service
        internal static INetMonRepositoryService GetNetMonRepositoryService() => new NetMonRepositoryService();
        #endregion Repository Service
    }
}
