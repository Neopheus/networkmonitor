﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using NetMon.Backend.Bootstrap;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;
using NetMon.Backend.ModuleServices.Interfaces;

namespace NetMon.Backend.ModuleServices
{
    /// <summary>
    /// 
    /// </summary>
    class NetMonRepositoryService : INetMonRepositoryService
    {
        #region ListFileModelService

        /// <summary>
        /// 
        /// </summary>
        private IListFilesModelService ListFilesModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IListFilesModelService GetCurrentListFilesModelService()
        {
            if (ListFilesModelService == null)
                ListFilesModelService = InitApplicationServices.GetListFilesModelService();
            return ListFilesModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFiles(string dirPath)
        {
            return await GetCurrentListFilesModelService().GetAllFiles(dirPath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesRecursive(string dirPath, List<string> filePaths = null)
        {
            return await GetCurrentListFilesModelService().GetAllFilesRecursive(dirPath, filePaths);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesWithExtension(string dirPath, string extension)
        {
            return await GetCurrentListFilesModelService().GetAllFilesWithExtension(dirPath, extension);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesWithExtensionRecursive(string dirPath, string extension, List<string> filePaths = null)
        {
            return await GetCurrentListFilesModelService().GetAllFilesWithExtensionRecursive(dirPath, extension, filePaths);
        }

        #endregion ListFileModelService

        #region NetMonConfigModelService

        /// <summary>
        /// 
        /// </summary>
        private INetMonConfigModelService NetMonConfigModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private INetMonConfigModelService GetCurrentNetMonConfigModelService()
        {
            if (NetMonConfigModelService == null)
                NetMonConfigModelService = InitApplicationServices.GetNetMonConfigModelService();
            return NetMonConfigModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public async Task<UniversalResult> SerializeToXML(NetMonConfig config)
        {
            return await GetCurrentNetMonConfigModelService().SerializeToXML(config);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlAsString"></param>
        /// <returns></returns>
        public async Task<UniversalResult> DeserializeFromXML(string xmlAsString)
        {
            return await GetCurrentNetMonConfigModelService().DeserializeFromXML(xmlAsString);
        }

        #endregion NetMonConfigModelService

        #region NetworkModelService

        /// <summary>
        /// 
        /// </summary>
        private INetworkModelService NetworkModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private INetworkModelService GetCurrentNetworkModelService()
        {
            if (NetworkModelService == null)
                NetworkModelService = InitApplicationServices.GetNetworkModelService();
            return NetworkModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public async Task<UniversalResult> IsAccessible(IPAddress ip)
        {
            return await GetCurrentNetworkModelService().IsAccessible(ip);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<UniversalResult> GetGatewayIp()
        {
            return await GetCurrentNetworkModelService().GetGatewayIp();
        }

        #endregion NetworkModelService

        #region ProcessModelService

        /// <summary>
        /// 
        /// </summary>
        private IProcessModelService ProcessModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IProcessModelService GetCurrentProcessModelService()
        {
            if (ProcessModelService == null)
                ProcessModelService = InitApplicationServices.GetProcessModelService();
            return ProcessModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="process"></param>
        /// <param name="arguementsAsString"></param>
        /// <returns></returns>
        public async Task<UniversalResult> StartProcess(string process, string arguementsAsString = "")
        {
            return await GetCurrentProcessModelService().StartProcess(process, arguementsAsString);
        }

        #endregion ProcessModelService

        #region ReadFileModelService

        /// <summary>
        /// 
        /// </summary>
        private IReadFileModelService ReadFileModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IReadFileModelService GetCurrentReadFileModelService()
        {
            if (ReadFileModelService == null)
                ReadFileModelService = InitApplicationServices.GetReadFileModelService();
            return ReadFileModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> FileExists(string filePath)
        {
            return await GetCurrentReadFileModelService().FileExists(filePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> ReadAllFromFile(string filePath)
        {
            return await GetCurrentReadFileModelService().ReadAllFromFile(filePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> ReadLinesFromFile(string filePath)
        {
            return await GetCurrentReadFileModelService().ReadLinesFromFile(filePath);
        }

        #endregion ReadFileModelService

        #region WriteFileModelService

        /// <summary>
        /// 
        /// </summary>
        private IWriteFileModelService WriteFileModelService;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IWriteFileModelService GetCurrentWriteFileModelService()
        {
            if (WriteFileModelService == null)
                WriteFileModelService = InitApplicationServices.GetWriteFileModelService();
            return WriteFileModelService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> CreateFile(string filePath)
        {
            return await GetCurrentWriteFileModelService().CreateFile(filePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        public async Task<UniversalResult> WriteLineToFile(string filePath, string textLine)
        {
            return await GetCurrentWriteFileModelService().WriteLineToFile(filePath, textLine);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        public async Task<UniversalResult> WriteLinesToFile(string filePath, List<string> textLines)
        {
            return await GetCurrentWriteFileModelService().WriteLinesToFile(filePath, textLines);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        public async Task<UniversalResult> AppendLineToFile(string filePath, string textLine)
        {
            return await GetCurrentWriteFileModelService().AppendLineToFile(filePath, textLine);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        public async Task<UniversalResult> AppendLinesToFile(string filePath, List<string> textLines)
        {
            return await GetCurrentWriteFileModelService().AppendLinesToFile(filePath, textLines);
        }
        
        #endregion WriteFileModelService
    }
}
