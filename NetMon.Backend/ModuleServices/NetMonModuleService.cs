﻿using System;
using System.IO;
using System.Threading.Tasks;
using NetMon.Backend.Bootstrap;
using NetMon.Backend.Models;
using NetMon.Backend.ModuleServices.Interfaces;

namespace NetMon.Backend.ModuleServices
{
    /// <summary>
    /// 
    /// </summary>
    public class NetMonModuleService : INetMonModuleService
    {
        #region Constructors

        /// <summary>
        /// The standard constructor.
        /// </summary>
        public NetMonModuleService()
        {

        }

        #endregion Constructors

        #region Variables

        //private readonly string DEFAULTCONFIGFILEPATH = $"{Directory.GetCurrentDirectory()}\\NetMonConfig.xml";
        private readonly string DEFAULTCONFIGFILEPATH = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\NetMon\\NetMonConfig.xml";
        private readonly string DEFAULTINTERNETIP = $"8.8.8.8";
        private readonly string DEFAULTINTERNETIPALTERNATIVE = $"8.8.6.6";

        #endregion Variables

        #region Get repository service

        /// <summary>
        /// The current NetMonRepositoryService
        /// </summary>
        private INetMonRepositoryService NetMonRepositoryService;

        /// <summary>
        /// Gets the current NetMonRepositoryService.
        /// </summary>
        /// <returns></returns>
        private INetMonRepositoryService GetCurrentNetMonRepositoryService()
        {
            if (NetMonRepositoryService == null)
                NetMonRepositoryService = InitApplicationServices.GetNetMonRepositoryService();
            return NetMonRepositoryService;
        }

        #endregion Get repository service

        #region Methods

        /// <summary>
        /// Loads the config from the file given path.
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> LoadConfig(string configPath = "")
        {
            UniversalResult result = null;
            string xmlAsString = string.Empty;
            NetMonConfig config = null;
            NetworkNodePool pool = null;

            if (string.IsNullOrEmpty(configPath))
                configPath = DEFAULTCONFIGFILEPATH;

            result = await GetCurrentNetMonRepositoryService().ReadAllFromFile(configPath);
            if (result?.IsSuccessAndObjectIsType<string>() == true)
                xmlAsString = result.ObjectAs<string>();
            else
                return result;

            result = await GetCurrentNetMonRepositoryService().DeserializeFromXML(xmlAsString);
            if (result?.IsSuccessAndObjectIsType<NetMonConfig>() == true)
                config = result.ObjectAs<NetMonConfig>();
            else
                return result;

            pool = new NetworkNodePool(config);

            return UniversalResult.DefaultSuccessResult($"Config successfully loaded.", pool);
        }

        /// <summary>
        /// Creates a config object from the given parameters and writes it into a file.
        /// </summary>
        /// <param name="targetDirectoryPath"></param>
        /// <param name="targetFileName"></param>
        /// <param name="filePaths"></param>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> SaveConfig(NetworkNodePool pool, string configPath = "")
        {
            UniversalResult result = null;
            string xmlAsString = string.Empty;

            if (string.IsNullOrEmpty(configPath))
                configPath = DEFAULTCONFIGFILEPATH;

            result = await GetCurrentNetMonRepositoryService().SerializeToXML(new NetMonConfig(pool));
            if (result?.IsSuccessAndObjectIsType<string>() == true)
                xmlAsString = result.ObjectAs<string>();
            else
                return result;

            result = await GetCurrentNetMonRepositoryService().WriteLineToFile(configPath, xmlAsString);
            if (result?.IsSuccess() == true)
                return UniversalResult.DefaultSuccessResult($"Config file successfully saved.", true);
            else
                return result;
        }

        /// <summary>
        /// Checks if a config file exists for the given path or the default path.
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> CheckConfig(string configPath = "")
        {
            UniversalResult result = null;
            bool fileExists = false;

            if (string.IsNullOrEmpty(configPath))
                configPath = DEFAULTCONFIGFILEPATH;

            result = await GetCurrentNetMonRepositoryService().FileExists(configPath);
            if (result?.IsSuccessAndObjectIsType<bool>() == true)
                fileExists = result.ObjectAs<bool>();
            else
                return result;

            return UniversalResult.DefaultSuccessResult($"Config successfully loaded.", fileExists);
        }

        /// <summary>
        /// Creates a default NetworkNodePool with internet ip as root node and gateway ip as following node.
        /// </summary>
        /// <returns></returns>
        public async Task<UniversalResult> CreateDefaultNetworkNodePool()
        {
            UniversalResult result = null;
            NetworkNodePool networkNodePool = new NetworkNodePool();
            string gatewayIp = string.Empty;

            result = await GetCurrentNetMonRepositoryService().GetGatewayIp();
            if (result.IsSuccessAndObjectIsType<string>())
                gatewayIp = result.ObjectAs<string>();
            else
                return result;

            NetworkNode internet = new NetworkNode()
            {
                Name = "Internet",
                Description = "Automatically generated node.",
                Ip = DEFAULTINTERNETIP,
                IpAlternative = DEFAULTINTERNETIPALTERNATIVE
            };

            NetworkNode gateway = new NetworkNode()
            {
                Name = "Gateway",
                Description = "Automatically generated node.",
                Ip = gatewayIp
            };

            internet.SetFollowingNode(gateway);
            networkNodePool.Add(internet);
            networkNodePool.Add(gateway);

            return UniversalResult.DefaultSuccessResult("Default NetworkNodePool successfully created.", networkNodePool);
        }

        /// <summary>
        /// Checks if a node is accessible with its ip or ipAlternative.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public async Task<UniversalResult> IsNetworkNodeAccessible(NetworkNode networkNode)
        {
            UniversalResult result = null;
            bool accessible = false;

            if (networkNode == null)
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonModuleService)}.{nameof(IsNetworkNodeAccessible)}(): networkNode is null.");

            result = await GetCurrentNetMonRepositoryService().IsAccessible(networkNode.GetIp());
            if (result.IsSuccessAndObjectIsType<bool>())
                accessible = result.ObjectAs<bool>();
            else
                return result;

            if (!accessible && !string.IsNullOrEmpty(networkNode.IpAlternative))
            {
                result = await GetCurrentNetMonRepositoryService().IsAccessible(networkNode.GetIpAlternative());
                if (result.IsSuccessAndObjectIsType<bool>())
                    accessible = result.ObjectAs<bool>();
                else
                    return result;
            }
            return UniversalResult.DefaultSuccessResult($"Executing {nameof(IsNetworkNodeAccessible)}() successfull.", accessible);
        }

        /// <summary>
        /// Checks the connectivity of a NetworkNodePool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        public async Task<UniversalResult> CheckPoolConnectivity(NetworkNodePool networkNodePool)
        {
            if (networkNodePool == null)
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonModuleService)}.{nameof(CheckPoolConnectivity)}(): networkNodePool is null.");

            return await CheckPoolConnectivity(networkNodePool.GetRootNode(), networkNodePool.Status);
        }

        /// <summary>
        /// Checks recursevliy if the NetworkNodePool is accessible.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <param name="networkStatus"></param>
        /// <returns></returns>
        private async Task<UniversalResult> CheckPoolConnectivity(NetworkNode networkNode, NetworkStatus networkStatus)
        {
            UniversalResult result = null;
            bool isAccessible = false;

            if (networkNode == null)
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonModuleService)}.{nameof(CheckPoolConnectivity)}(): networkNode is null.");

            if (networkStatus == null)
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonModuleService)}.{nameof(CheckPoolConnectivity)}(): networkStatus is null.");

            result = await IsNetworkNodeAccessible(networkNode);
            if (result.IsSuccessAndObjectIsType<bool>())
                isAccessible = result.ObjectAs<bool>();
            else
                return result;

            if (isAccessible)
            {
                if (networkNode.HasPreviousNode())
                    networkStatus.SetErrorAt(networkNode.PreviousNode);
                else
                    networkStatus.SetErrorAt(null);
            }
            else
            {
                if (networkNode.HasFollowingNode())
                    return await CheckPoolConnectivity(networkNode.FollowingNode, networkStatus);
                else
                    networkStatus.SetErrorAt(networkNode);
            }

            return UniversalResult.DefaultSuccessResult($"Executing {nameof(CheckPoolConnectivity)}() successfull.", networkStatus);
        }

        /// <summary>
        /// Checks the connectivity of a NetworkNodePool (if NetworkNodePool is null, it will be loaded from config).
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        public async Task<UniversalResult> LoadAndCheckPoolConnectivity(NetworkNodePool networkNodePool = null)
        {
            UniversalResult result = null;

            if (networkNodePool == null)
            {
                bool configExists = false;

                result = await CheckConfig();
                if (result.IsSuccessAndObjectIsType<bool>())
                    configExists = result.ObjectAs<bool>();
                else
                    return result;

                if (configExists)
                {
                    result = await LoadConfig();
                    if (result.IsSuccessAndObjectIsType<NetworkNodePool>())
                        networkNodePool = result.ObjectAs<NetworkNodePool>();
                    else
                        return result;
                }
                else
                {
                    result = await CreateDefaultNetworkNodePool();
                    if (result.IsSuccessAndObjectIsType<NetworkNodePool>())
                        networkNodePool = result.ObjectAs<NetworkNodePool>();
                    else
                        return result;

                    bool configSaved = false;
                    result = await SaveConfig(networkNodePool);
                    if (result.IsSuccessAndObjectIsType<bool>())
                        configSaved = result.ObjectAs<bool>();
                    else
                        return result;
                }
            }

            if (networkNodePool != null)
            {
                result = await CheckPoolConnectivity(networkNodePool);
                if (result.IsSuccessAndObjectIsType<Backend.Models.NetworkStatus>())
                    networkNodePool.Status = result.ObjectAs<Backend.Models.NetworkStatus>();
                else
                    return result;
            }

            return UniversalResult.DefaultSuccessResult($"Executing {nameof(LoadAndCheckPoolConnectivity)}() successfull.", networkNodePool);
        }

        #endregion Methods

        #region Enums



        #endregion Enums

    }
}
