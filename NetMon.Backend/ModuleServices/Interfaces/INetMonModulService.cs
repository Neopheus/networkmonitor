﻿using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModuleServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface INetMonModuleService
    {
        /// <summary>
        /// Loads the config from the file given path.
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        Task<UniversalResult> LoadConfig(string configPath = "");

        /// <summary>
        /// Creates a config object from the given parameters and writes it into a file.
        /// </summary>
        /// <param name="pool"></param>
        /// <param name="configPath"></param>
        /// <returns></returns>
        Task<UniversalResult> SaveConfig(NetworkNodePool pool, string configPath = "");

        /// <summary>
        /// Checks if a config file exists for the given path or the default path.
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        Task<UniversalResult> CheckConfig(string configPath = "");

        /// <summary>
        /// Creates a default NetworkNodePool with internet ip as root node and gateway ip as following node.
        /// </summary>
        /// <returns></returns>
        Task<UniversalResult> CreateDefaultNetworkNodePool();

            /// <summary>
        /// Checks the connectivity of a NetworkNodePool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        Task<UniversalResult> CheckPoolConnectivity(NetworkNodePool networkNodePool);

        /// <summary>
        /// Checks the connectivity of a NetworkNodePool (if NetworkNodePool is null, it will be loaded from config).
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        Task<UniversalResult> LoadAndCheckPoolConnectivity(NetworkNodePool networkNodePool = null);
    }
}
