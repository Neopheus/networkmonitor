﻿using NetMon.Backend.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NetMon.Backend.ModuleServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface INetMonRepositoryService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFiles(string dirPath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesRecursive(string dirPath, List<string> filePaths = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesWithExtension(string dirPath, string extension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesWithExtensionRecursive(string dirPath, string extension, List<string> filePaths = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        Task<UniversalResult> SerializeToXML(NetMonConfig config);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlAsString"></param>
        /// <returns></returns>
        Task<UniversalResult> DeserializeFromXML(string xmlAsString);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        Task<UniversalResult> IsAccessible(IPAddress ip);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<UniversalResult> GetGatewayIp();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="process"></param>
        /// <param name="arguementsAsString"></param>
        /// <returns></returns>
        Task<UniversalResult> StartProcess(string process, string arguementsAsString = "");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> FileExists(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> ReadAllFromFile(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> ReadLinesFromFile(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> CreateFile(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        Task<UniversalResult> WriteLineToFile(string filePath, string textLine);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        Task<UniversalResult> WriteLinesToFile(string filePath, List<string> textLines);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        Task<UniversalResult> AppendLineToFile(string filePath, string textLine);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        Task<UniversalResult> AppendLinesToFile(string filePath, List<string> textLines);
    }
}
