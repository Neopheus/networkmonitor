﻿using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;
using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class NetworkModelService : INetworkModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public NetworkModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public async Task<UniversalResult> IsAccessible(IPAddress ip)
        {
            Ping ping = new Ping();
            try
            {
                if (ip == null)
                    return UniversalResult.DefaultErrorResult($"{nameof(NetworkModelService)}.{nameof(IsAccessible)}(): Parameter 'ip' is null.");

                if (!NetworkInterface.GetIsNetworkAvailable())
                    return UniversalResult.DefaultErrorResult($"{nameof(NetworkModelService)}.{nameof(IsAccessible)}(): NetworkInterface not available.");

                bool accessible = ping.Send(ip).Status == IPStatus.Success;

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(NetworkModelService)}.{nameof(IsAccessible)}(): Opearation successfull.", accessible));
            }
            catch (PingException ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(NetworkModelService)}.{nameof(IsAccessible)}(): A PingException occured ({ex.Message}).");
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(NetworkModelService)}.{nameof(IsAccessible)}(): An Exception occured ({ex.Message}).");
            }
            finally
            {
                ping.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<UniversalResult> GetGatewayIp()
        {
            try
            {
                IPAddress gatewayIp = NetworkInterface
                    .GetAllNetworkInterfaces()
                    .Where(n => n.OperationalStatus == OperationalStatus.Up)
                    .Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                    .SelectMany(n => n.GetIPProperties()?.GatewayAddresses)
                    .Select(g => g?.Address)
                    .Where(a => a != null)
                    .Where(a => a.AddressFamily == AddressFamily.InterNetwork)
                    .Where(a => Array.FindIndex(a.GetAddressBytes(), b => b != 0) >= 0)
                    .FirstOrDefault();

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(NetworkModelService)}.{nameof(GetGatewayIp)}(): Opearation successfull.", gatewayIp.ToString()));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(NetworkModelService)}.{nameof(GetGatewayIp)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
