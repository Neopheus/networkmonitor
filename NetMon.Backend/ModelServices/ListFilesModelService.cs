﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class ListFilesModelService : IListFilesModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public ListFilesModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFiles(string dirPath)
        {
            List<string> filePaths = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFiles)}(): Directory path was empty.");

                if (!Directory.Exists(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFiles)}(): Directory '{dirPath}' does not exist.");

                string[] files = Directory.GetFiles(dirPath);
                for (int i = 0; i < files.Length; i++)
                    filePaths.Add(files[i]);

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFiles)}(): Opearation successfull.", filePaths));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFiles)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesRecursive(string dirPath, List<string> filePaths = null)
        {
            if (filePaths == null)
                filePaths = new List<string>();

            try
            {
                if (string.IsNullOrEmpty(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesRecursive)}(): Directory path was empty.");

                if (!Directory.Exists(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesRecursive)}(): Directory '{dirPath}' does not exist.");

                string[] files = Directory.GetFiles(dirPath);
                for (int i = 0; i < files.Length; i++)
                    filePaths.Add(files[i]);

                string[] directories = Directory.GetDirectories(dirPath);
                for (int i = 0; i < directories.Length; i++)
                {
                    var result = await GetAllFilesRecursive(directories[i], filePaths);
                    if (result?.ResultStatus == true && result.ResultObject != null)
                        filePaths = (List<string>)result.ResultObject;
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesRecursive)}(): Opearation successfull.", filePaths);
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesRecursive)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesWithExtension(string dirPath, string extension)
        {
            List<string> filePaths = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtension)}(): Directory path was empty.");

                if (!Directory.Exists(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtension)}(): Directory '{dirPath}' does not exist.");

                if (extension == null)
                    extension = string.Empty;

                string[] files = Directory.GetFiles(dirPath, $"*{extension}");
                for (int i = 0; i < files.Length; i++)
                    filePaths.Add(files[i]);

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtension)}(): Opearation successfull.", filePaths));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtension)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public async Task<UniversalResult> GetAllFilesWithExtensionRecursive(string dirPath, string extension, List<string> filePaths = null)
        {
            if (filePaths == null)
                filePaths = new List<string>();

            try
            {
                if (string.IsNullOrEmpty(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtensionRecursive)}(): Directory path was empty.");

                if (!Directory.Exists(dirPath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtensionRecursive)}(): Directory '{dirPath}' does not exist.");

                if (extension == null)
                    extension = string.Empty;

                string[] files = Directory.GetFiles(dirPath, $"*{extension}");
                for (int i = 0; i < files.Length; i++)
                    filePaths.Add(files[i]);

                string[] directories = Directory.GetDirectories(dirPath);
                for (int i = 0; i < directories.Length; i++)
                {
                    var result = await GetAllFilesWithExtensionRecursive(directories[i], extension, filePaths);
                    if (result?.ResultStatus == true && result.ResultObject != null)
                        filePaths = (List<string>)result.ResultObject;
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtensionRecursive)}(): Opearation successfull.", filePaths);
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ListFilesModelService)}.{nameof(GetAllFilesWithExtensionRecursive)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
