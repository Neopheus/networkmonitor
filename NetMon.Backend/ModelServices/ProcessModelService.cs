﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessModelService : IProcessModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public ProcessModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="process"></param>
        /// <param name="arguementsAsString"></param>
        /// <returns></returns>
        public async Task<UniversalResult> StartProcess(string process, string arguementsAsString = "")
        {
            try
            {
                if (string.IsNullOrEmpty(process))
                    return UniversalResult.DefaultErrorResult($"{nameof(ProcessModelService)}.{nameof(StartProcess)}(): process was empty.");

                Process.Start(process, arguementsAsString);

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(ProcessModelService)}.{nameof(StartProcess)}(): Opearation successfull."));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ProcessModelService)}.{nameof(StartProcess)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
