﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IListFilesModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFiles(string dirPath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesRecursive(string dirPath, List<string> filePaths = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesWithExtension(string dirPath, string extension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="extension"></param>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        Task<UniversalResult> GetAllFilesWithExtensionRecursive(string dirPath, string extension, List<string> filePaths = null);
    }
}
