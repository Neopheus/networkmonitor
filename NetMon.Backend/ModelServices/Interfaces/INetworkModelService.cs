﻿using NetMon.Backend.Models;
using System.Net;
using System.Threading.Tasks;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface INetworkModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        Task<UniversalResult> IsAccessible(IPAddress ip);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<UniversalResult> GetGatewayIp();
    }
}
