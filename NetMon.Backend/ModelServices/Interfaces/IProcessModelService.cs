﻿using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProcessModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process"></param>
        /// <param name="arguementsAsString"></param>
        /// <returns></returns>
        Task<UniversalResult> StartProcess(string process, string arguementsAsString = "");
    }
}
