﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWriteFileModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> CreateFile(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        Task<UniversalResult> WriteLineToFile(string filePath, string textLine);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        Task<UniversalResult> WriteLinesToFile(string filePath, List<string> textLines);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        Task<UniversalResult> AppendLineToFile(string filePath, string textLine);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        Task<UniversalResult> AppendLinesToFile(string filePath, List<string> textLines);
    }
}
