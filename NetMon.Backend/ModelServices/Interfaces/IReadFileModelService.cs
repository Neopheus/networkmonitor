﻿using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReadFileModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> FileExists(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> ReadAllFromFile(string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        Task<UniversalResult> ReadLinesFromFile(string filePath);
    }
}
