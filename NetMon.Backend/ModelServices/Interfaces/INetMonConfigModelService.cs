﻿using System.Threading.Tasks;
using NetMon.Backend.Models;

namespace NetMon.Backend.ModelServices.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface INetMonConfigModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        Task<UniversalResult> SerializeToXML(NetMonConfig config);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlAsString"></param>
        /// <returns></returns>
        Task<UniversalResult> DeserializeFromXML(string xmlAsString);
    }
}
