﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class NetMonConfigModelService : INetMonConfigModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public NetMonConfigModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public async Task<UniversalResult> SerializeToXML(NetMonConfig config)
        {
            try
            {
                if (config == null)
                    return UniversalResult.DefaultErrorResult($"{nameof(NetMonConfigModelService)}.{nameof(SerializeToXML)}(): Given config was null.");

                var xmlAsString = string.Empty;
                var objectToSerialize = config;
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(NetMonConfig));
                using (var stringWriter = new StringWriter())
                {
                    using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter))
                    {
                        xmlSerializer.Serialize(xmlWriter, objectToSerialize);
                        xmlAsString = stringWriter.ToString();
                    }
                }

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(NetMonConfigModelService)}.{nameof(SerializeToXML)}(): Opearation successfull.", xmlAsString));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonConfigModelService)}.{nameof(SerializeToXML)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlAsString"></param>
        /// <returns></returns>
        public async Task<UniversalResult> DeserializeFromXML(string xmlAsString)
        {
            try
            {
                if (string.IsNullOrEmpty(xmlAsString))
                    return UniversalResult.DefaultErrorResult($"{nameof(NetMonConfigModelService)}.{nameof(DeserializeFromXML)}(): Given string was null.");

                NetMonConfig config = null;
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(NetMonConfig));
                using (var stringReader = new StringReader(xmlAsString))
                {
                    config = xmlSerializer.Deserialize(stringReader) as NetMonConfig;
                }

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(NetMonConfigModelService)}.{nameof(DeserializeFromXML)}(): Opearation successfull.", config));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(NetMonConfigModelService)}.{nameof(DeserializeFromXML)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
