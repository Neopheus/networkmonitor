﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class WriteFileModelService : IWriteFileModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public WriteFileModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> CreateFile(string filePath)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(CreateFile)}(): File path was empty.");

                File.Create(filePath);

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(WriteFileModelService)}.{nameof(CreateFile)}(): Opearation successfull."));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(CreateFile)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        public async Task<UniversalResult> WriteLineToFile(string filePath, string textLine)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(WriteLineToFile)}(): File path was empty.");

                if (textLine == null)
                    textLine = string.Empty;

                using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
                {
                    await writer.WriteLineAsync(textLine);
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(WriteFileModelService)}.{nameof(WriteLineToFile)}(): Opearation successfull.");
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(WriteLineToFile)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        public async Task<UniversalResult> WriteLinesToFile(string filePath, List<string> textLines)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(WriteLinesToFile)}(): File path was empty.");

                if (textLines == null)
                    textLines = new List<string>();

                using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
                {
                    foreach (string textLine in textLines)
                    {
                        await writer.WriteLineAsync(textLine);
                    }
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(WriteFileModelService)}.{nameof(WriteLinesToFile)}(): Opearation successfull.");
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(WriteLinesToFile)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLine"></param>
        /// <returns></returns>
        public async Task<UniversalResult> AppendLineToFile(string filePath, string textLine)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(AppendLineToFile)}(): File path was empty.");

                if (textLine == null)
                    textLine = string.Empty;

                using (StreamWriter writer = File.AppendText(filePath))
                {
                    await writer.WriteLineAsync(textLine);
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(WriteFileModelService)}.{nameof(AppendLineToFile)}(): Opearation successfull.");
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(AppendLineToFile)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="textLines"></param>
        /// <returns></returns>
        public async Task<UniversalResult> AppendLinesToFile(string filePath, List<string> textLines)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(AppendLinesToFile)}(): File path was empty.");

                if (textLines == null)
                    textLines = new List<string>();

                using (StreamWriter writer = File.AppendText(filePath))
                {
                    foreach (string textLine in textLines)
                    {
                        await writer.WriteLineAsync(textLine);
                    }
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(WriteFileModelService)}.{nameof(AppendLinesToFile)}(): Opearation successfull.");
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(WriteFileModelService)}.{nameof(AppendLinesToFile)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
