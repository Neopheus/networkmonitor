﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NetMon.Backend.Models;
using NetMon.Backend.ModelServices.Interfaces;

namespace NetMon.Backend.ModelServices
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadFileModelService : IReadFileModelService
    {
        /// <summary>
        /// 
        /// </summary>
        public ReadFileModelService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> FileExists(string filePath)
        {
            bool fileExists = false;
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(FileExists)}(): File path was empty.");

                fileExists = File.Exists(filePath);

                return await Task.FromResult(UniversalResult.DefaultSuccessResult($"{nameof(ReadFileModelService)}.{nameof(FileExists)}(): Opearation successfull.", fileExists));
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(FileExists)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> ReadAllFromFile(string filePath)
        {
            string fileContent = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadAllFromFile)}(): File path was empty.");

                if (!File.Exists(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadAllFromFile)}(): File '{filePath}' does not exist.");

                using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
                {
                    fileContent = await reader.ReadToEndAsync();
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(ReadFileModelService)}.{nameof(ReadAllFromFile)}(): Opearation successfull.", fileContent);
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadAllFromFile)}(): An Exception occured ({ex.Message}).");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<UniversalResult> ReadLinesFromFile(string filePath)
        {
            List<string> fileContent = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadLinesFromFile)}(): File path was empty.");

                if (!File.Exists(filePath))
                    return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadLinesFromFile)}(): File '{filePath}' does not exist.");

                using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
                {
                    while (reader.Peek() >= 0)
                    {
                        fileContent.Add(await reader.ReadLineAsync());
                    }
                }

                return UniversalResult.DefaultSuccessResult($"{nameof(ReadFileModelService)}.{nameof(ReadLinesFromFile)}(): Opearation successfull.", fileContent);
            }
            catch (Exception ex)
            {
                return UniversalResult.DefaultErrorResult($"{nameof(ReadFileModelService)}.{nameof(ReadLinesFromFile)}(): An Exception occured ({ex.Message}).");
            }
        }
    }
}
