﻿using System.Net;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// Extensions for NetworkNode
    /// </summary>
    public static class NetworkNodeExtensions
    {
        /// <summary>
        /// Checks if two nodes are equal by comparing the ips.
        /// </summary>
        /// <param name="nodeA"></param>
        /// <param name="nodeB"></param>
        /// <returns></returns>
        public static bool Equals(this NetworkNode nodeA, NetworkNode nodeB)
        {
            return (nodeB != null) && (nodeB.Ip == nodeA.Ip);
        }

        /// <summary>
        /// Checks if a NetworkNode has a valid Ip.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static bool HasValidIp(this NetworkNode networkNode)
        {
            return IPAddress.TryParse(networkNode.Ip, out IPAddress ip);
        }

        /// <summary>
        /// Gets the Ip as IPAddress.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static IPAddress GetIp(this NetworkNode networkNode)
        {
            IPAddress.TryParse(networkNode.Ip, out IPAddress ip);
            return ip;
        }

        /// <summary>
        /// Checks if a NetworkNode has a valid IpAlternative.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static bool HasValidIpAlternative(this NetworkNode networkNode)
        {
            return IPAddress.TryParse(networkNode.IpAlternative, out IPAddress ipAlternative);
        }

        /// <summary>
        /// Gets the IpAlternative as IPAddress.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static IPAddress GetIpAlternative(this NetworkNode networkNode)
        {
            IPAddress.TryParse(networkNode.IpAlternative, out IPAddress ip);
            return ip;
        }

        /// <summary>
        /// Sets the alternative Ip of the NetworkNode.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <param name="ip"></param>
        public static void SetIpAlternative(this NetworkNode networkNode, string ip)
        {
            if ((ip != null) && (ip != networkNode.Ip))
                networkNode.IpAlternative = ip;
            else
                networkNode.IpAlternative = null;
        }

        /// <summary>
        /// Sets the following node of a NetworkNode and also the previous node of the following NetworkNode.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <param name="followingNode"></param>
        public static void SetFollowingNode(this NetworkNode networkNode, NetworkNode followingNode)
        {
            if (followingNode != null && !followingNode.Equals(networkNode))
            {
                networkNode.FollowingNode = followingNode;
                followingNode.PreviousNode = networkNode;
            }
            else
                networkNode.FollowingNode = null;
        }

        /// <summary>
        /// Sets the previous node of a NetworkNode and also the following node of the previous NetworkNode.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <param name="followingNode"></param>
        public static void SetPreviousNode(this NetworkNode networkNode, NetworkNode previousNode)
        {
            if (previousNode != null && !previousNode.Equals(networkNode))
            {
                networkNode.PreviousNode = previousNode;
                previousNode.FollowingNode = networkNode;
            }
            else
                networkNode.PreviousNode = null;
        }

        /// <summary>
        /// Checks if the NetworkNode has a following node.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static bool HasFollowingNode(this NetworkNode networkNode)
        {
            return networkNode.FollowingNode is NetworkNode;
        }

        /// <summary>
        /// Checks if the NetworkNode has a previous node.
        /// </summary>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static bool HasPreviousNode(this NetworkNode networkNode)
        {
            return networkNode.PreviousNode is NetworkNode;
        }
    }
}
