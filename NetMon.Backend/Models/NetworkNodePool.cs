﻿using System.Collections.Generic;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// Represents a pool of NetworkNodes
    /// </summary>
    public class NetworkNodePool
    {
        private List<NetworkNode> pool;
        private NetworkStatus status;

        /// <summary>
        /// 
        /// </summary>
        public NetworkNodePool()
        {
            Pool = new List<NetworkNode>();
            Status = new NetworkStatus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public NetworkNodePool(NetMonConfig config)
        {
            Pool = new List<NetworkNode>();
            Status = new NetworkStatus();
            if (config?.RootNode != null)
            {
                NetworkNode currentNode = config.RootNode;
                NetworkNode previousNode = null;
                while (currentNode is NetworkNode)
                {
                    currentNode.SetPreviousNode(previousNode);
                    Pool.Add(currentNode);
                    previousNode = currentNode;
                    currentNode = currentNode.FollowingNode;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<NetworkNode> Pool
        {
            get => pool;
            set => pool = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public NetworkStatus Status
        {
            get => status;
            set => status = value;
        }
    }
}
