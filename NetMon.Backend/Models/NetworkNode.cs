﻿using System;
using System.ComponentModel;
using System.Net;
using System.Xml.Serialization;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// Represents a node in the network
    /// </summary>
    [Serializable]
    [DefaultValue(null)]
    [XmlRoot(ElementName = "NetworkNode")]
    public class NetworkNode
    {
        private string name = String.Empty;
        private string description = String.Empty;
        private string ip = null;
        private string ipAlternative = null;
        private NetworkNode followingNode = null;
        private NetworkNode previousNode = null;

        /// <summary>
        /// 
        /// </summary>
        public NetworkNode()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="ipAlternative"></param>
        /// <param name="followingNode"></param>
        public NetworkNode(string ip, string name = "nameless", string description = "no description", string ipAlternative = null, NetworkNode followingNode = null)
        {
            Name = name;
            Description = description;
            Ip = ip;
            IpAlternative = ipAlternative;
            FollowingNode = followingNode;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get => name;
            set => name = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get => description;
            set => description = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Ip
        {
            get => ip;
            set => ip = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public string IpAlternative
        {
            get => ipAlternative;
            set => ipAlternative = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public NetworkNode FollowingNode
        {
            get => followingNode;
            set => followingNode = value;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        public NetworkNode PreviousNode
        {
            get => previousNode;
            set => previousNode = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{name} ({ip})";
        }
    }
}
