﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// Prepresents the status of a network.
    /// </summary>
    public class NetworkStatus
    {
        private NetworkNode errorAt;
        private NetworkNode lastErrorAt;
        private bool hasChanged;
        private DateTime lastChange;
        private bool initialized;

        /// <summary>
        /// 
        /// </summary>
        public NetworkStatus()
        {
            ErrorAt = null;
            LastErrorAt = null;
            HasChanged = false;
            Initialized = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public NetworkNode ErrorAt
        {
            get => errorAt;
            set => errorAt = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public NetworkNode LastErrorAt
        {
            get => lastErrorAt;
            set => lastErrorAt = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasChanged
        {
            get => hasChanged;
            set => hasChanged = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastChange
        {
            get => lastChange;
            set => lastChange = value;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Initialized
        {
            get => initialized;
            set => initialized = value;
        }
    }
}
