﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// Extensions for NetworkNodePool
    /// </summary>
    public static class NetworkNodePoolExtensions
    {
        /// <summary>
        /// Gets the root node from the pool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        public static NetworkNode GetRootNode(this NetworkNodePool networkNodePool)
        {
            foreach (NetworkNode node in networkNodePool.Pool)
            {
                if (node.PreviousNode == null)
                    return node;
            }
            return null;
        }

        /// <summary>
        /// Adds a node to the pool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static bool Add(this NetworkNodePool networkNodePool, NetworkNode node)
        {
            if (!networkNodePool.Pool.Exists(x => x.Ip == node.Ip))
            {
                networkNodePool.Pool.Add(node);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Adds a node and all of its subnodes to the pool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <param name="node"></param>
        public static void AddRecursive(this NetworkNodePool networkNodePool, NetworkNode node)
        {
            networkNodePool.Add(node);
            if (node.FollowingNode != null) networkNodePool.AddRecursive(node.FollowingNode);
        }

        /// <summary>
        /// Removes a node from the pool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static bool Remove(this NetworkNodePool networkNodePool, NetworkNode node)
        {
            if (networkNodePool.Pool.Exists(x => x.Ip == node.Ip))
            {
                networkNodePool.Pool.Remove(node);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Checks if a node with the same ip already exists in the pool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static bool Exists(this NetworkNodePool networkNodePool, NetworkNode node)
        {
            return networkNodePool.Pool.Exists(x => x.Ip == node.Ip);
        }

        /// <summary>
        /// Checks if the current NetworkNodePool status has null in the ErrorAt property.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        public static bool IsConnected(this NetworkNodePool networkNodePool)
        {
            return networkNodePool.Status.ErrorAt == null;
        }

        /// <summary>
        /// Checks if the current NetworkNodePool status has null in the LastErrorAt property.
        /// </summary>
        /// <param name="networkNodePool"></param>
        /// <returns></returns>
        public static bool WasConnected(this NetworkNodePool networkNodePool)
        {
            return networkNodePool.Status.LastErrorAt == null;
        }

        /// <summary>
        /// Resets the status of the NetworkNodePool.
        /// </summary>
        /// <param name="networkNodePool"></param>
        public static void ResetStatus(this NetworkNodePool networkNodePool)
        {
            networkNodePool.Status = new NetworkStatus();
        }
    }
}
