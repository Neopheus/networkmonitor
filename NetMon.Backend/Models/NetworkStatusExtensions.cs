﻿using System;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// 
    /// </summary>
    public static class NetworkStatusExtensions
    {
        /// <summary>
        /// Sets the ErrorAt property and checks if it has changed since the last time.
        /// </summary>
        /// <param name="networkStatus"></param>
        /// <param name="networkNode"></param>
        /// <returns></returns>
        public static bool SetErrorAt(this NetworkStatus networkStatus, NetworkNode networkNode)
        {
            bool equal = (networkStatus.ErrorAt == null && networkNode == null) ||
                         (networkStatus.ErrorAt is NetworkNode && networkStatus.ErrorAt.Equals(networkNode));
            if (equal && !networkStatus.Initialized)
            {
                networkStatus.HasChanged = false;
            }
            else
            {
                networkStatus.HasChanged = true;
                networkStatus.LastChange = DateTime.Now;
            }

            networkStatus.LastErrorAt = networkStatus.ErrorAt;
            networkStatus.ErrorAt = networkNode;
            networkStatus.Initialized = false;

            return networkStatus.HasChanged;
        }
    }
}
