﻿using System;

namespace NetMon.Backend.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalResult
    {
        /// <summary>
        /// 
        /// </summary>
        public UniversalResult()
        {
            Level = UniversalResultLevel.Message;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ResultStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UniversalResultLevel Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object ResultObject { get; set; }

        /// <summary>
        /// Creates a standard success result object with the given message and object.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="resultObject"></param>
        /// <returns></returns>
        public static UniversalResult DefaultSuccessResult(string message = "", object resultObject = null)
        {
            return new UniversalResult()
            {
                ResultStatus = true,
                ResultObject = resultObject,
                ErrorCode = "0",
                Message = message,
                Level = UniversalResultLevel.Message
            };
        }

        /// <summary>
        /// Creates a standard error result object with the given message.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static UniversalResult DefaultErrorResult(string errorMessage = "", int errorCode = 1, UniversalResultLevel level = UniversalResultLevel.Error)
        {
            return new UniversalResult()
            {
                ResultStatus = false,
                ResultObject = null,
                ErrorCode = errorCode.ToString(),
                Message = errorMessage,
                Level = level
            };
        }

        /// <summary>
        /// Creates a standard error result object with the given message.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static UniversalResult NotImplementedErrorResult(string currentClass, string currentMethod)
        {
            return new UniversalResult()
            {
                ResultStatus = false,
                ResultObject = null,
                ErrorCode = "1",
                Message = $"{currentClass}.{currentMethod}(): Not yet implemented.",
                Level = UniversalResultLevel.Error
            };
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class UniversalResultExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="universalResult"></param>
        /// <returns></returns>
        public static bool IsSuccess(this UniversalResult universalResult)
        {
            return universalResult.ResultStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="universalResult"></param>
        /// <returns></returns>
        public static bool IsSuccessAndObjectNotNull(this UniversalResult universalResult)
        {
            return universalResult.ResultStatus == true && universalResult.ResultObject != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="universalResult"></param>
        /// <returns></returns>
        public static bool IsSuccessAndObjectIsType<T>(this UniversalResult universalResult)
        {
            return universalResult.ResultStatus == true && universalResult.ResultObject != null && universalResult.ResultObject is T;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="universalResult"></param>
        /// <returns></returns>
        public static bool IsFail(this UniversalResult universalResult)
        {
            return !universalResult.ResultStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="resultObject"></param>
        public static void SetSuccessResult(this UniversalResult universalResult, string message = "", object resultObject = null)
        {
            universalResult.ResultStatus = true;
            universalResult.ResultObject = resultObject;
            universalResult.ErrorCode = "0";
            universalResult.Message = message;
            universalResult.Level = UniversalResultLevel.Message;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="universalResult"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <param name="level"></param>
        public static void SetErrorResult(this UniversalResult universalResult, string errorMessage = "", int errorCode = 1, UniversalResultLevel level = UniversalResultLevel.Error)
        {
            universalResult.ResultStatus = false;
            universalResult.ResultObject = null;
            universalResult.ErrorCode = errorCode.ToString();
            universalResult.Message = errorMessage;
            universalResult.Level = level;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="universalResult"></param>
        /// <returns></returns>
        public static T ObjectAs<T>(this UniversalResult universalResult)
        {
            return (T)Convert.ChangeType(universalResult.ResultObject, typeof(T));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum UniversalResultLevel
    {
        Message = 0,
        Notification = 1,
        Warning = 2,
        Error = 3,
    }
}
