﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace NetMon.Backend.Models
{
    [Serializable]
    [DefaultValue(null)]
    [XmlRoot(ElementName = "NetMonConfig")]
    public class NetMonConfig
    {
        public NetMonConfig()
        {

        }

        public NetMonConfig(NetworkNode rootNode)
        {
            RootNode = rootNode;
        }

        public NetMonConfig(NetworkNodePool pool)
        {
            RootNode = pool.GetRootNode();
        }

        [DefaultValue(null)]
        [XmlElement(ElementName = "RootNode")]
        public NetworkNode RootNode { get; set; }
    }
}
